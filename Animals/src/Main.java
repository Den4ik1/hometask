import java.util.*;
import java.io.*;

public class Main {
    public static Scanner in;

    public static Animal addAnimal() {
        in = new Scanner(System.in);
        Animal animal = null;
        System.out.println("Choose your animal (cat, dog, hamster)");
        String animalType = in.nextLine();
        switch (animalType) {
            case "cat":
                animal = new Cat();
                break;
            case "dog":
                animal = new Dog();
                break;
            case "hamster":
                animal = new Hamster();
        }
        return animal;
    }

    public static void main(String[] args) throws PowerExeption, IOException {
        in = new Scanner(System.in);
        System.out.println("Pets amount = ");
        int n = in.nextInt();
        Map<String, Animal> animals = new HashMap<String, Animal>();
        for (int i = 0; i < n; i++) {
            Animal animal = addAnimal();
            System.out.println("Enter name");
            String name = in.nextLine();
            animals.put(name, animal);
        }
        System.out.println("Choose your pet ");
        String name = in.nextLine();
        Animal animal = (Animal) animals.get(name);
        System.out.println("What to do? (eat, play, sleep, add, change)");
        String task = in.nextLine();
        while (true) {
            try {
                switch (task) {
                    case "change":
                        System.out.println("Choose your pet ");
                        String name1 = in.nextLine();
                        Animal animal1 = (Animal) animals.get(name);
                        break;
                    case "add":
                        animal = addAnimal();
                        System.out.println("Enter name");
                        String newAnimalName = in.nextLine();
                        animals.put(newAnimalName, new Animal());
                        break;
                    case "eat":
                        animal.setPower(animal.eat());
                        break;
                    case "sleep":
                        animal.setPower(animal.sleep());
                        break;
                    case "play":
                        animal.setPower(animal.play());
                        break;
                }
                animal.isAlive();
            } catch (PowerExeption e) {
                System.err.println("Deth");
                animals.remove(name);
                ;
            }
        }
    }
}